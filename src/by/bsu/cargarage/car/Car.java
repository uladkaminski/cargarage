package by.bsu.cargarage.car;

import java.util.Date;

public class Car {
	private String model;
	private int year;
	private Type type;
	public Car() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Car(String model, int year, Type type) {
		super();
		this.model = model;
		this.year = year;
		this.type = type;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		
		if(year>1700){
			this.year = year;	
		}else new IllegalArgumentException();
		
	}
	public Type getType() {
		return type;
	}
	public void setType(Type type) {
		this.type = type;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((model == null) ? 0 : model.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + year;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Car other = (Car) obj;
		if (model == null) {
			if (other.model != null)
				return false;
		} else if (!model.equals(other.model))
			return false;
		if (type != other.type)
			return false;
		if (year != other.year)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Car [model=" + model + ", year=" + year + ", type=" + type
				+ "]";
	}
	
}
