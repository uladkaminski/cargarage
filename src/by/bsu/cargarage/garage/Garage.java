package by.bsu.cargarage.garage;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import by.bsu.cargarage.car.Car;
import by.bsu.cargarage.car.Type;

public class Garage {
	private List<Car> garage;

	public Garage() {
		super();
		garage = new ArrayList<Car>();
		// TODO Auto-generated constructor stub
	}

	
	public List<Car> getGarage() {
		return garage;
	}


	public void setGarage(List<Car> garage) {
		this.garage = garage;
	}


	public Garage(List<Car> garage) {
		super();
		this.garage = garage;
	}
	public static Garage parseFromJSON(String file) throws Exception{
		Garage garage = new Garage();
		JSONParser parser = new JSONParser();
		FileReader reader = new FileReader(file);
		JSONObject object = (JSONObject) parser.parse(reader);
		JSONArray array = (JSONArray) object.get("garage");
		
		for(Object obj : array){
			 JSONObject innerObj = (JSONObject) obj;
			Car car = new Car();
			car.setModel((String)innerObj.get("model"));
			Long i =(Long) innerObj.get("year");
			car.setYear(i.intValue());
			
			car.setType(Type.valueOf((String)innerObj.get("type")));
			garage.addCar(car);
		}
		return garage;
	}
	public void parseToJSON(String file){
		JSONObject object = new JSONObject();
		JSONArray array = new JSONArray();
		for(Car car : garage){
			array.add(car);
		}
		
		object.put("number", garage.size());
		object.put("cars",array.toJSONString());
		try{
			FileWriter out = new FileWriter(new File(file));
	        try{
	        	out.write(object.toString());
	        }finally{
	        	out.close();
	        }
			} catch(IOException e) {
				
	            e.getStackTrace();
	        } 
		
	}
	public void addCarByUser(){
		Car car = new Car();
		Scanner scan = new Scanner(System.in);
		System.out.println("Vvedite model avto");
		car.setModel(scan.next());
		System.out.println("Vvedite god avto");
		car.setYear(scan.nextInt());
		System.out.println("Vvedite type avto");
		String type = scan.next();
		car.setType(Type.valueOf(type.toUpperCase()));
		garage.add(car);
		System.out.println("Thank you!");
		scan.close();
	}
	public void addCar(Car car){
		garage.add(car);
	}

	@Override
	public String toString() {
		return "Garage [garage=" + garage + "]";
	}
}
